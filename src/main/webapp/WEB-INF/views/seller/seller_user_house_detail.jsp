<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/include/header.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>조가튼 세상</title>
</head>
<link href="https://fonts.googleapis.com/css?family=Do+Hyeon&display=swap" rel="stylesheet">
<script type="text/javascript" src="http://code.jquery.com/jquery-1.7.min.js">
</script>
<script type="text/javascript">
$(function(){
	// 클릭시 view 호출
	$(".stage_gra_black_vertical").click(function(){
		var encKey = $(this).find("input[type=hidden]").val();
		go_view(encKey, event);
	});
	
	$(".records").click(function(){
		var encKey = $(this).find("input[type=checkbox]").val();
		go_view(encKey, event);
	});
});

//상세 view 호출
function go_view(key, e){
	
	if(e != undefined && e != '') {
		if(e.target.localName == "input") {
			return;
		}
	}
	
	if(key != '') {
		$("#hiddenHouseKey").val(key);
	}else {
		$("#hiddenHouseKey").val("");
	}
	
	var frmData = document.form2;
	  
	frmData.action="<c:url value='/seller/seller_user_house_detail'/>"; 
	frmData.method="post"; 
	frmData.submit();
}

</script>
<style>
#contents{
	width: 100%;
	height: 100%;
	overflow: hidden;
	float: center;
}

#contents h3{
	margin-left: 20%;
}

#content{
	width: 70%;
	height: 100%;
	float: center;
	margin: auto;
}

.rightDiv{
	float: right;
	width: 75%;
	height: 100%;
	overflow: hidden;
	margin: auto;
	border: 1px solid #f7323f;
	border-radius: 5px;
}

.stage_gra_black_vertical{
	width: 45%;
	height: 280px;
	border: 1px solid white;
	background-size: 100% 100%;
	margin: auto;
	cursor: pointer;
	float: left;
}

.stage_gra_black_content{
	width: 50%;
	height: 280px;
	float: right;
	
}

.houseName{
	font-family: 'Do Hyeon', sans-serif;
	font-size: 27px;
	color: black;
	margin: 0 10px 0 28px;
}

.houseReview{
	font-family: 'Jua', sans-serif;
	font-size: 17px;
	color: #FFA726;
	margin: 0 10px 0 28px;
}

.addres{
	font-family: 'Do Hyeon', sans-serif;
	font-size: 23px;
	color: black;
	margin: 5px 10px 10px 28px;
}

.houseText{
	font-family: 'Do Hyeon', sans-serif;
	font-size: 23px;
	color: rgba(0,0,0,0.56);
	background-color: #F4F4F4;
	margin: 5px 10px 10px 28px;
}

</style>
<body>
	<div id="contents">
		<div id="content" class="sub_wrap">
			<br>
			<br>
			<div class="rightDiv">
				<div class="stage_gra_black_vertical" style="background-image: URL(<c:url value="${houseVo.files.get(0).filePath }" />);">
				</div>
				<div class="stage_gra_black_content">
					<table class="board_list3">
						<th class="houseName"><c:out value="${houseVo.houseNm }" /></th>
						<tr>
							<td class="houseReview">9.1 추천해요  리뷰 394개</td>
						</tr>
						<tr>
							<td class="addres"><c:out value="${houseVo.houseAddress }" /></td>
						</tr>
						<tr>
							<td class="houseText">최저가보장, 예약취소가능</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
	</div>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
</body>
</html>