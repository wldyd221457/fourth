<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="assets/css/main.css" />
</head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script type="text/javascript">
	console.log(${houses});
</script>
<body>
	
		<div id="main">
					<div class="inner">
					
					<!-- Boxes -->
						<div class="thumbnails">
						<c:forEach var="item" items="${houses }">
							<div class="box">
								<a href="" class="image fit"><img width="350" height="260" src="<c:url value="${item.filePath }" />"></a>
								<div class="inner">
									<h3>${item.houseNm }</h3>
									<a href="#" onclick="location.href='/study/user/detail'"class="button fit" id="detailRoom">상세보기</a>
								</div>
							</div>
						</c:forEach>
						</div>
					</div>
	</div>
</body>
</html>