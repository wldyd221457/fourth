<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>관리자</title>
</head>

<style>
    .houseTb {
        border-collapse: collapse;
        text-align: center;
    }

    tr,
    th,
    td {
        border: 1px solid black;
        width: 50px;
    }

    td {
        width: 250px;
    }

    .wd70 {
        width: 70px;
    }

    #searchDiv {
        margin: 10px 0 10px 0;
    }

    #searchDiv select {
        height: 30px;
    }

    #searchDiv input[type=text] {
        height: 21px;
    }

    #houseBox {
        width: 70vw;
        margin: 15vh 15vw 6vw 15vw;
    }
</style>

<body>
    <jsp:include page="/WEB-INF/views/include/header.jsp" />
    <jsp:include page="/WEB-INF/views/admin/adminHeader.jsp" />

    <div id="houseBox">
        <h2>숙소리스트</h2>

        <div id="searchDiv">
            <form id="searchUser" action="/study/admin/manageHouse/houseList" method="POST">
                <select name="searchType" id="searchType">
                    <option <c:if test="${ searchType == 'id' }"> selected </c:if> value="id">등록자 ID로 검색</option>
                    <option <c:if test="${ searchType == 'title' }"> selected </c:if> value="title">숙소명으로 검색</option>
                </select>
                <input type="text" name="searchText" id="searchText"
                    value="<c:if test='${ !empty searchText }' > ${searchText}</c:if>">
                <input type="submit" value="검색">
            </form>
        </div>

        <c:choose>
            <c:when test="${!empty houseList}">
                <table class="houseTb">
                    <tr>
                        <th>등록번호</th>
                        <th>숙소이름</th>
                        <th>주소</th>
                        <th>승인여부</th>
                        <th>등록자</th>
                    </tr>
                    <c:forEach var="house" items="${houseList}">
                        <tr>
                            <td class="wd70">${house.houseSeq}</td>
                            <td><a
                                    href="/study/admin/manageHouse/houseModify?houseSeq=${house.houseSeq}">${house.houseNm}</a>
                            </td>
                            <td>${house.houseAddress}</td>
                            <td>
                                <c:if test="${!empty house.houseStatus}">
                                    <c:choose>
                                        <c:when test="${house.houseStatus == '0' }">대기중</c:when>
                                        <c:when test="${house.houseStatus == '1' }">승인완료</c:when>
                                        <c:when test="${house.houseStatus == '2' }">보류</c:when>
                                        <c:when test="${house.houseStatus == '3' }">거절</c:when>
                                        <c:otherwise>
                                            지정된 상태값이 아님
                                        </c:otherwise>
                                    </c:choose>
                                </c:if>
                                <c:if test="${empty house.houseStatus}">
                                    상태값이 없음
                                </c:if>
                            </td>

                            <td>
                                <c:if test="${!empty house.user}">
                                    ${house.user.userId}
                                </c:if>
                            </td>
                        </tr>
                    </c:forEach>
                </table>
            </c:when>

            <c:otherwise>
                <h2>조회 결과가 없습니다.</h2>
            </c:otherwise>
        </c:choose>
    </div>
</body>

</html>

<script type="text/javascript">

</script>