<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>관리자</title>
</head>

<style>
    .houseTb { border-collapse: collapse; }
    tr, th, td { border:1px solid black; }   
    th { width:100px; font-size:1.2em; font-weight:bold; background-color:darkgray; color:white; text-align:center;}
    td { width:300px; text-align:left;}
    #subBtn {margin:10px 0 10px 0; float:right;}
    #modBox {    width: 70vw;
        margin: 15vh 15vw 6vw 15vw;}
    #houseForm {width:55vw;}
</style>
<body>
    <jsp:include page="/WEB-INF/views/include/header.jsp"/>    
    <jsp:include page="/WEB-INF/views/admin/adminHeader.jsp"/>
    <div id="modBox">
        <h2>House 상세페이지</h2>

        <form id="houseForm"  method="POST">
            <table class="houseTb">
                <tr>
                    <input type="hidden" name="houseSeq" value="${house.houseSeq}">
                    <th>등록번호</th><td>${house.houseSeq}</td>
                    <th>숙소이름</th><td>${house.houseNm}</td>
                </tr>
                <tr>
                    <th>주소</th><td colspan=4>${house.houseAddress}</td>
                </tr>
                <tr>
                    <th>등록자</th>
                    <td>
                        <c:if test="${!empty house.user}">${house.user.userNm}</c:if>
                    </td>
                    <th>연락처</th>
                    <td>
                        <c:if test="${!empty house.housePh}">${house.housePh}</c:if>
                    </td>
                </tr>
                <tr>
                    <th>지역</th>
                    <td>
                        <c:if test="${!empty house.local}">${house.local.localSeq}</c:if>
                    </td>
                    <th>방 정보</th>
                    <td>
                        <c:if test="${!empty house.roomTypes}">${house.roomTypes.get(0).roomTypeTitle}</c:if>
                    </td>
                </tr>
                <tr>
                    <th>승인여부</th>
                    <td>
                        <select name="houseStatus" id="houseStatus">
                                <option <c:if test="${!empty house.houseStatus}"> <c:if test="${ house.houseStatus == 0 }" > selected </c:if></c:if> value="0">대기중</option>
                                <option <c:if test="${!empty house.houseStatus}"> <c:if test="${ house.houseStatus == 1 }" > selected </c:if></c:if> value="1">승인완료</option>
                                <option <c:if test="${!empty house.houseStatus}"> <c:if test="${ house.houseStatus == 2 }" > selected </c:if></c:if> value="2">보류</option>
                                <option <c:if test="${!empty house.houseStatus}"> <c:if test="${ house.houseStatus == 3 }" > selected </c:if></c:if> value="3">거절</option>
                        </select>
                    </td>
                    <th></th>
                    <td></td>
                </tr>
                <tr>
                    <th colspan=1 >방 이미지</th>
                    <td colspan=3 >
                        <c:if test="${!empty house.files}">
                            <!--
                                c:url은 url에 자동으로 contextPath를 붙여주는 기능을 한다.
                                c:url을 사용하지않고 contextPath를 붙이기 위해서는 아래와 같이 사용하면 된다.
                                
                                <img src="${pageContext.request.contextPath}${house.files.get(0).filePath}"/>
                            -->
                            <img src="<c:url value= '${house.files.get(0).filePath}'/>"/>

                        </c:if>
                    </td>
                </tr>
                <tr>
                    <th colspan=4>소개글</th>
                </tr>
                <tr>
                    <td colspan=4 rowspan=3>
                        <c:if test="${!empty house.user}">${house.houseContent}</c:if>
                    </td>
                </tr>
            </table>
            <input id="subBtn" type="button" value="수정하기" onclick="updateHouse();">
        </form>
    </div>
</body>
</html>

<script type="text/javascript">
    function updateHouse(){
        $("#houseForm").attr("action","/study/admin/manageHouse/updateHouseModi");
        $("#houseForm").submit();
    }
</script>
