<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>관리자</title>
</head>

<style>
    table {
        border-collapse: collapse;
    }

    .userTb tr,
    th,
    td {
        border: 1px solid black;
    }

    .userTb {
        text-align: center;
    }

    .userTb td {
        width: 10vw;
        height: 33px;
    }

    #searchDiv {
        margin: 10px 0 10px 0;
    }

    #searchDiv select {
        height: 30px;
    }

    #searchDiv input[type=text] {
        height: 21px;
    }

    #h2Title {
        text-align: left;
        font-size: 2em;
    }

    #userBox {
        width: 50vw;
        margin: 12vh 0 6vw 25vw;
    }
</style>

<body>
    <jsp:include page="/WEB-INF/views/include/header.jsp" />
    <jsp:include page="/WEB-INF/views/admin/adminHeader.jsp" />
    <!-- 
        "User(
            userSeq=2
            userId=user01
            userPwd=111
            userNm=usernm01
            userPh=01011111111
            userDt=2019-11-30 00:46:41.0
            modDt=2019-11-30 00:46:41.0
            houses=[]
            books=[]
            auth=null
        )"
     -->
    <div id="userBox">
        <h2 id="h2Title">유저 권한 수정</h2>
        <div id="searchDiv">
            <form id="searchUser" action="/study/admin/manageAuth/userList" method="POST">
                <select name="searchType" id="searchType">
                    <option <c:if test="${ searchType == 'id' }"> selected </c:if> value="id">ID로 검색</option>
                    <option <c:if test="${ searchType == 'name' }"> selected </c:if> value="name">회원 이름으로 검색</option>
                </select>
                <input type="text" name="searchText" id="searchText"
                    value="<c:if test='${ !empty searchText }' > ${searchText}</c:if>">
                <input type="submit" value="검색">
            </form>
        </div>

        <table class="userTb">
            <tr>
                <th>유저번호</th>
                <th>유저아이디</th>
                <th>유저이름</th>
                <th>유저권한</th>
                <th>수정버튼</th>
            </tr>
            <c:choose>
                <c:when test="${!empty userList}">
                    <c:forEach var="user" items="${userList}" varStatus="status">
                        <input type="hidden" id="user${user.userSeq}AuthSeq" value="${user.auth.authSeq}">
                        <tr>
                            <td>${user.userSeq}</td>
                            <td>${user.userId}</td>
                            <td>${user.userNm}</td>
                            <td>
                                <select id="user${user.userSeq}auth">

                                    <option <c:if test="${ user.auth.authRole == 'ROLE_AUTH0000' }"> selected </c:if>
                                        value="ROLE_AUTH0000" > 관리자 </option>
                                    <option <c:if test="${ user.auth.authRole == 'ROLE_AUTH0100' }"> selected </c:if>
                                        value="ROLE_AUTH0100" > 판매자 </option>
                                    <option <c:if test="${ user.auth.authRole == 'ROLE_AUTH0101' }"> selected </c:if>
                                        value="ROLE_AUTH0101" > 사용자 </option>
                                    <option <c:if test="${ empty user.auth.authRole }"> selected </c:if>
                                        value="ROLE_AUTH0101" > 권한 없음 </option>
                                </select>
                            </td>
                            <td> <input type="button" onclick="updateAuth('${user.userSeq}');" value="수정하기"> </td>
                        </tr>
                    </c:forEach>
                </c:when>

                <c:otherwise>
                    <h2>조회 결과가 없습니다.</h2>
                </c:otherwise>
            </c:choose>
        </table>
    </div>

    <form id="userForm">
        <input type="hidden" name="userSeq" id="userSeq">
        <input type="hidden" name="authSeq" id="authSeq">
        <input type="hidden" name="authRole" id="authRole">
        <input type="hidden" name="sort" id="sort" value="desc">
    </form>
</body>

</html>

<script type="text/javascript">
    function updateAuth(userSeq) {
        if (userSeq != '') {
            $('#userSeq').val(userSeq);

            var userAuth = $("#user" + userSeq + "auth").val();
            var userAuthSeq = $("#user" + userSeq + "AuthSeq").val();

            $('#authRole').val(userAuth);

            userAuthSeq = userAuthSeq != '' ? userAuthSeq : '0';
            $('#authSeq').val(parseInt(userAuthSeq));

        } else {
            alert("다시 시도해주세요!");
            return false;
        }

        if (!confirm("변경하시겠습니까?")) {
            return false;
        }

        $("#userForm").attr("action", "/study/admin/manageAuth/updateUserAuth");
        $("#userForm").submit();
    }
</script>