<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>관리자</title>
</head>

<style>
    .menuBox {
        width: 80vw;
        height: 300px;
        margin: 6vh auto 0 auto;
        text-align: center;
    }

    .menuBox input[type=button] {
        width: 173px;
        height: 120px;
        margin: 12vh 2vw 15px 10px;
        border-radius: 11%;
        font-weight: bold;
        font-size: 1.7em;
        background-color: darkviolet;
        color: white;
    }
    h2, div, input {font-family: 'Do Hyeon', sans-serif;}
    #h2Title{text-align:left;font-size:2em;}
</style>

<body>
    <jsp:include page="/WEB-INF/views/include/header.jsp" />
    <div class="menuBox">
        <h2 id="h2Title">어드민 홈</h2>
        <input class="menuBtn" type="button" value='유저권한 관리' onclick="location.href='/study/admin/manageAuth/userList'">
        <input class="menuBtn" type="button" value='숙소 관리' onclick="location.href='/study/admin/manageHouse/houseList'">
        <input class="menuBtn" type="button" value='카테고리 관리' onclick="alert('준비중입니다!');">
        <input class="menuBtn" type="button" value='설정' onclick="alert('준비중입니다!');">
    </div>
</body>

</html>

<script type="text/javascript">
</script>