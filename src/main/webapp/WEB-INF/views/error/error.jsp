<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/include/header.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link href="https://fonts.googleapis.com/css?family=Do+Hyeon&display=swap" rel="stylesheet">
<title>로그인 성공</title>
</head>
<style>

.stage_gra_black_vertical {
	width: 100%;
	height: 2000px;
	background-color: #f7323f;
	float: center;
}
.mainImage{
	text-align: center;
}
.stage_gra_black_vertical p{
	font-family: 'Do Hyeon', sans-serif;
	font-size: 34px;
	color: white;
}

</style>
<body>
	<div class="stage_gra_black_vertical">
		<div class="mainImage">
			<img src="<c:url value="/resources/images/mainPoto.PNG" />"
				id="logo_plus" style="cursor: pointer" alt="야놀자">
			<p>사용자 권한을 확인해주세요.</p>
		</div>
	</div>
</body>
</html>