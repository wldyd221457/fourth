<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html xmlns:th="http://www.thymeleaf.org">
<head>
<meta charset="UTF-8">
</head>
<script type="text/javascript"
	src="http://code.jquery.com/jquery-1.7.min.js">
	
</script>
<script type="text/javascript">
	$(function() {

		localStorage.clear();

		$('#admin_login').click(function() {
			login();
		});
	});

	function onKeyDown() {
		if (event.keyCode == 13) {
			login();
		}
	}

	function login() {
		if ($("#adminId").val() == null || $("#adminId").val() == '') {
			alert("아이디를 입력해주세요.");
			return;
		}
		if ($("#adminPw").val() == null || $("#adminPw").val() == '') {
			alert("비밀번호를 입력해주세요.");
			return;
		}

		$("#loginStep").val("1");

		$.ajax({
			url : './j_spring_security_check',
			data : $('form input').serialize(),
			type : 'post',
			async : false,
			success : function(data) {
				console.log(data);
				if (data.resultCode == "0000") {
					loginSuccess(data.result.homeUrl);
				} else {
					alert("아이디 혹은 비밀번호를 확인해 주세요");
					console.log("실패");
					return;
				}
			},
			error : function(error) {
				alert("에러")
				console.log(error);
			}
		});
	}

	function loginSuccess(homeUrl) {
		location.href = "<c:url value='"+ homeUrl+"'/>";
	}
</script>
<style>
#login {
	width: 1005px;
	padding-top: 40px;
	margin: 0px auto;
	padding-top: 20px;
}

#login fieldset {
	overflow: hidden;
	width: 230px;
	margin: 0 auto;
}

legend, caption, #skip_navi {
	position: absolute;
	overflow: hidden;
	visibility: hidden;
	height: 0;
	font-size: 0;
	line-height: 0;
	z-index: -1;
}

#login fieldset ul li {
	padding: 8px 13px;
}

#login input[type='text'], #login input[type='password'] {
	width: 180px;
	border: none;
	font-weight: bold;
	font-size: 20px;
	color: #d0d9e1;
	line-height: 24px;
}

img, input, select, textarea, button {
	border: 0;
	vertical-align: middle;
}

ul {
	list-style-type: none;
	margin: 0;
	padding: 0;
}

#login li {
	border: 1px solid #BDBDBD;
	border-radius: 3px;
}

#logo {
	width: 120px;
	height: 30px;
}

fieldset {
	border: 1px solid white;
}
</style>
<body>
	<form id="form" name="form" method="post"
		action="/j_spring_security_check">
		<input type="hidden" id="authKey" name="authKey" /> <input
			type="hidden" id="adminCtn" name="adminCtn" /> <input type="hidden"
			id="loginStep" name="loginStep" /> <input type="hidden" id="closeYn"
			name="closeYn" /> <input type="hidden" id="pwChangeYn"
			name="pwChangeYn" />
		<div id="wrapper">
			<div id="login">
				<img src="<c:url value="/resources/images/logo.PNG" />" id="logo" style="cursor: pointer" alt="야놀자"> <br>
				<br>
				<br>
				<br>
				<br>
				<br>
				<br>
				<div style="diplay: inline-block; text-align: center; margin-top: 100px; font-size: 30px; font-weight: bold"></div>
				<fieldset>
					<legend>로그인 입력</legend>
					<ul style="margin-bottom: 20px;">
						<li><input type="text" id="adminId" name="adminId"
							placeholder="아이디" onKeyDown="onKeyDown();" autocomplete="off" />
						</li>
						<li><input type="password" id="adminPw" name="adminPw"
							placeholder="비밀번호" onKeyDown="onKeyDown();" autocomplete="off" />
						</li>
					</ul>
					<img src="<c:url value="/resources/images/btn_login.gif" />"
						id="admin_login" style="cursor: pointer" alt="로그인">
				</fieldset>
			</div>
		</div>
	</form>
</body>
</html>