package com.kh.study.controller.seller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.kh.study.model.House;
import com.kh.study.model.User;
import com.kh.study.model.common.BusinessInputVo;
import com.kh.study.model.common.BusinessResultVo;
import com.kh.study.model.common.EncDecKeyVo;
import com.kh.study.services.HousesService;
import com.kh.study.util.OpsConsts;

@Controller
@RequestMapping("/seller")
public class SellerController {
	
	@Autowired
	private HousesService housesService;
	
	@RequestMapping(value = "/seller_house_list")
	public String houseList(@ModelAttribute("BusinessInputVo") BusinessInputVo businessInputVo, 
			@RequestParam(value = "searchName", required = false) String searchName, ModelMap modelMap, HttpSession session) throws Exception {
		
		User userVo = this.getRegUserVo(session);
		
		if (businessInputVo.getCurrentPage() == 0) businessInputVo.setCurrentPage(1);
		businessInputVo.setLinesPercentPage(OpsConsts.DEFAULT_ROW_SIZE);
		businessInputVo.setRowFirstNum((businessInputVo.getCurrentPage() - 1) * businessInputVo.getLinesPercentPage() + 1);
		businessInputVo.setRowLastNum(businessInputVo.getCurrentPage() * businessInputVo.getLinesPercentPage());
		
		BusinessResultVo businessResultVo = new BusinessResultVo();
		businessResultVo.setListCount(OpsConsts.DEFAULT_LIST_COUNT);
		businessResultVo.setListCountTotal(housesService.getHousesListCount(businessInputVo, userVo.getUserSeq()));
		
		List<House> houseVos = housesService.getUserHousesList(businessInputVo, userVo.getUserSeq());
		
		EncDecKeyVo encDecKeyVo = new EncDecKeyVo();
		List<String> keyList = new ArrayList<String>();
		
		for (House vo : houseVos) {
			keyList.add(String.valueOf(vo.getHouseSeq()));
		}
		encDecKeyVo.setEncKeyList(keyList);
		
		modelMap.addAttribute("EncDecKeyVo", encDecKeyVo);
		modelMap.addAttribute("BusinessResultVo", businessResultVo);
		modelMap.addAttribute("houseVos", houseVos);
		
		return "/seller/seller_house_list";
	}
	
	@RequestMapping(value = "/seller_user_house_detail")
	public String userDetailHouse(@ModelAttribute("BusinessInputVo") BusinessInputVo businessInputVo, 
			@RequestParam(value = "encKey", required = false) String encKey, ModelMap modelMap, HttpSession session) throws Exception {
		
		//User userVo = this.getRegUserVo(session);
		
		long houseKey = 0;
		
		houseKey = Long.parseLong(encKey);
		
		House houseVo = housesService.getUserDetailHouse(businessInputVo, houseKey);
		
		modelMap.addAttribute("houseVo", houseVo);
		
		return "/seller/seller_user_house_detail";
	}

	private User getRegUserVo(HttpSession session) {
		long userSeq = (long) session.getAttribute("userSeq");
		String userNm = (String) session.getAttribute("userNm");
		User regVo = new User();
		regVo.setUserSeq(userSeq);
		regVo.setUserNm(userNm);
		return regVo;
	}
	
}
