package com.kh.study.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import java.util.*;
import javax.servlet.http.HttpServletRequest;
import com.kh.study.model.User;
import com.kh.study.model.Auth;
import com.kh.study.model.House;
import com.kh.study.repository.AuthRepository;
import com.kh.study.repository.HouseRepository;
import com.kh.study.repository.UserRepository;

@Controller
@RequestMapping("/admin")
public class ManagerController {

	@Autowired
	private UserRepository userRepository;
	@Autowired
	private AuthRepository authRepository;
	@Autowired
	private HouseRepository houseRepository;
	
	@RequestMapping(value="/home")	//사용자가 요청할 URL 맵핑
	public String goAdminHome() {
		return "/admin/adminHome";
	}
	
	@RequestMapping(value="/manageAuth/userList")
	public String getAllUsers(Model model, HttpServletRequest request) {
		//검색관련 (검색 컬럼, 검색어)
		String searchType = request.getParameter("searchType") != null ? request.getParameter("searchType").toString() : "";
		String searchText = request.getParameter("searchText") != null ? request.getParameter("searchText").toString() : "";

		List<User> userList = null ;
			
		if ( !"".equals(searchType) && !"".equals(searchText)) {
			if ( "id".equals(searchType) ) { 
				//조회해온 userList 같은 데이터를 Syso 또는 toString() 했을 때 StackOverflowError가 자주 일어남
				userList = userRepository.findByUserIdLikeCustom(searchText);
			} else if ( "name".equals(searchType) ) { 
				userList = userRepository.findByUserNameLikeCustom(searchText); 
			} 
		} else {
			userList = userRepository.findAll();
		}
		
		model.addAttribute("searchType", searchType);
		model.addAttribute("searchText", searchText);
		model.addAttribute("userList", userList); 
		return "/admin/manageAuth/userList";
	}

	@RequestMapping(value="/manageAuth/updateUserAuth")
	public String updateUserAuth(Auth auth, User user ) {
		auth.setUser(user);
		authRepository.save(auth);
		return "redirect:/admin/manageAuth/userList";
	}

	@RequestMapping(value="/manageHouse/houseList")
	public String selectHouseList(Model model, HttpServletRequest request) {
		//검색관련 (검색 컬럼, 검색어)
		String searchType = request.getParameter("searchType") != null ? request.getParameter("searchType").toString().trim() : "";
		String searchText = request.getParameter("searchText") != null ? request.getParameter("searchText").toString().trim() : "";

		List<House> houseList = null;

		if ( !"".equals(searchType) && !"".equals(searchText)) {
			if ( "id".equals(searchType) ) { 
				//조회해온 houseList 같은 데이터를 Syso 또는 toString() 했을 때 StackOverflowError가 자주 일어남
				houseList = houseRepository.findByUserIdCustom(searchText);
			} else if ( "title".equals(searchType) ) { 
				houseList = houseRepository.findByHouseNmCustom(searchText);
			} 
		} else {
			houseList = houseRepository.findAll();
		}

		model.addAttribute("searchType", searchType);
		model.addAttribute("searchText", searchText);
		model.addAttribute("houseList", houseList);

		return "/admin/manageHouse/houseList";
	}

	@RequestMapping(value="/manageHouse/houseModify")
	public String selectHouseModify(Model model, HttpServletRequest request) {
		String houseSeq = request.getParameter("houseSeq") != null ? request.getParameter("houseSeq").toString() : "";
		if ( "".equals(houseSeq) ) {
			return "redirect:/study/admin/manageHouse/houseList";
		}
		House house = null;
		house = houseRepository.findOneByHouseSeqCustom( Long.parseLong(houseSeq) );

		model.addAttribute("house", house);
		
		return "/admin/manageHouse/houseModify";
	}

	@RequestMapping(value="/manageHouse/updateHouseModi")
	public String updateHouseModi(House house) {
		houseRepository.updateHouseStatusCustom(house.getHouseStatus(), house.getHouseSeq()) ;

		return "redirect:/admin/manageHouse/houseList";
	}
}
