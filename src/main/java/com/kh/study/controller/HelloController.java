package com.kh.study.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.kh.study.model.Auth;
import com.kh.study.model.File;
import com.kh.study.model.House;
import com.kh.study.model.User;
import com.kh.study.repository.AuthRepository;
import com.kh.study.repository.FileRepository;
import com.kh.study.repository.HouseRepository;
import com.kh.study.repository.UserRepository;


@Controller
public class HelloController {
	
	@Autowired
	AuthRepository authRepository;
	@Autowired
	HouseRepository houseRepository;
	@Autowired
	UserRepository userRepository;
	@Autowired
	FileRepository fileRepository;
	//최초 화면 매핑
	@RequestMapping(value="/")
	public ModelAndView main() {
		
		ModelAndView mv = new ModelAndView();
		mv.setViewName("/user/header");
		
		return mv;
	}
	
	@RequestMapping(value="/user")
	public ModelAndView user() {
		List<House> houses = new ArrayList<House>();
		List<File> files = new ArrayList<File>();
		File file = new File();
		houses = houseRepository.findAll();
		
	
		  JSONArray houseArray = new JSONArray(); 
		  for(int i = 0; i< houses.size(); i++){ 
		  JSONObject sObject = new JSONObject(); 
		  sObject.put("houseSeq",houses.get(i).getHouseSeq()); 
		  sObject.put("houseAddress",houses.get(i).getHouseAddress()); 
		  sObject.put("houseNm",houses.get(i).getHouseNm()); 
		  sObject.put("housePh",houses.get(i).getHousePh()); 
		  sObject.put("houseContent",houses.get(i).getHouseContent()); 
		  files = houses.get(i).getFiles(); 
		  for(int j= 0; j < files.size(); j++) { 
			  sObject.put("filePath",files.get(j).getFilePath()); 
			} 
		  houseArray.add(sObject); 
		  }
		
		ModelAndView mv = new ModelAndView();
		mv.addObject("houses",houseArray);
		
		mv.setViewName("/user/user");
		return mv;
	}
	@RequestMapping(value="/user/detail")
	public ModelAndView detail() {
		ModelAndView mv = new ModelAndView();
		
		mv.setViewName("/user/detail");
		return mv;
	}
}
