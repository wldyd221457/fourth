package com.kh.study.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kh.study.model.House;
import com.kh.study.model.common.BusinessInputVo;
import com.kh.study.repository.HousesRepository;

@Service
public class HousesService {
	
	@Autowired
	private HousesRepository housesRepository;

	public int getHousesListCount(BusinessInputVo businessInputVo, long userSeq) {
		return (int) housesRepository.count(userSeq);
	}

	public List<House> getUserHousesList(BusinessInputVo businessInputVo, long userSeq) {
		return housesRepository.getUserHousesList(userSeq);
	}

	public House getUserDetailHouse(BusinessInputVo businessInputVo, long encKey) {
		return housesRepository.getUserDetailHouse(encKey);
	}

}
