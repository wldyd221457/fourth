package com.kh.study.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kh.study.model.User;
import com.kh.study.repository.UsersRepository;

@Service
public class LoginService {
	
	@Autowired
	private UsersRepository usersRepository;

	public User selectUsersById(String id) {
		return usersRepository.selectAdminUsersById(id);
	}

}
