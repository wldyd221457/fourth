package com.kh.study.util;

import org.springframework.stereotype.Component;

@Component
public final class Consts {
	
	public final static String REQUEST_SUCCESS = "S";
	
	/**
     * 어드민 타입 (ROLE_SUPER_ADMIN : 슈퍼어드민)
     */
    public final static String SUPER_ADMIN_ROLE_NAME = "ROLE_AUTH0000";
    public final static String CMS_ADMIN_ROLE_NAME = "ROLE_AUTH0001";
    public final static String STAT_ADMIN_ROLE_NAME = "ROLE_AUTH0002"; 
    public final static String BIZ_ADMIN_WITH_CHANNEL_ROLE_NAME = "ROLE_AUTH0100"; 
    public final static String BIZ_ADMIN_NO_CHANNEL_ROLE_NAME = "ROLE_AUTH0101";
}
