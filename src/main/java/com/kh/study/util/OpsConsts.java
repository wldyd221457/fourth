package com.kh.study.util;

public class OpsConsts {
	
	/** 페이지당 row 수 **/
    public final static int DEFAULT_ROW_SIZE = 10;
    /** 페이지당 page Count 수 **/
    public final static int DEFAULT_LIST_COUNT = 10;
    
}
