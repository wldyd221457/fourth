package com.kh.study.common.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configuration.EnableGlobalAuthentication;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.kh.study.interceptor.AuthProvider;
import com.kh.study.interceptor.LoginFailureHandler;
import com.kh.study.interceptor.LoginSuccessHandler;

@Configuration
@EnableWebSecurity
@EnableGlobalAuthentication
@ComponentScan(basePackages = {"com.kh.*"})
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter {
	
	//로그인과 관련된  Provider 의존성 주입
	@Autowired
	AuthProvider authProvider;
	
	//로그인 실패시 이동할 Handler 의존성 주입
	@Autowired
	LoginFailureHandler authFailureHandler;
	
	//로그인 성공시 이동할 Handler 의존성 주입
	@Autowired
	LoginSuccessHandler authSuccessHandler;
	
	
	//비밀번호 BCrypt 암호화를 위한 Bean 등록
	@Bean
    public BCryptPasswordEncoder passwordEncoder() {
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        return bCryptPasswordEncoder;
    }

	//로그인 필요없이 특정 path("/**/*.css", "/**/*.png", "/**/*.jpg", "/**/*.gif")에 접근이 가능하도록 설정
	@Override
	public void configure(WebSecurity web) {
		web.ignoring().antMatchers("/**/*.css", "/**/*.png", "/**/*.jpg", "/**/*.gif");
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {

		//로그인 설정
		http.authorizeRequests()
			//permitAll() 메소드는 어떠한 보안 요구 없이 요청을 허용해준다.
			.antMatchers("/login", "/login/**", "/users/**", "/error", "/error/**").permitAll()
			//hasAnyRole() 메소드는 여러개의 권한 설정가능
			.antMatchers("/common/**").hasAnyRole("AUTH0000", "AUTH0100", "AUTH0101", "AUTH0001")
			.antMatchers("/seller/**").hasAnyRole("AUTH0000", "AUTH0100")
			//hasRole() 메소드는 하나의 권한만 설정가능
			.antMatchers("/admin/**").hasRole("AUTH0000")
		.and()
			//csrf 비활성화
			.csrf().disable()
			//로그인 폼 사용
			.formLogin()
			//로그인 URL 설정 이 설정이 없으면 스프링 시큐리티에서 제공해주는 로그인 페이지로 이동
			.loginPage("/login")
			//authProvider에서 로그인 성공시 이동할 Handler 설정
			.successHandler(authSuccessHandler)
			//authProvider에서 로그인 실패시 이동할 Handler 설정
			.failureHandler(authFailureHandler)
			//해당 URL 요청이 들어오면  authProvider로 로그인 정보를 전달하여 로그인 로직 수행될 수 있도록 설정
			.loginProcessingUrl("/j_spring_security_check")
			//이 설정을 통해 사용자가 전달한 key 값이 아이디인지 알 수 있다.(login.jsp 정의) 
			.usernameParameter("adminId")
			//이 설정을 통해 사용자가 전달한 key 값이 패스워드인지 알 수 있다.(login.jsp 정의) 
			.passwordParameter("adminPw")
		.and()    
			//로그아웃 관련 설정
			.logout()
			//로그아웃을 요청할 URL
			.logoutUrl("/logout")
			.invalidateHttpSession(true)
			//로그아웃 성공시 보낼 페이지 설정
			.logoutSuccessUrl("/login")
		.and()
			//로그인 프로세스가 진행될 provider 설정
			.authenticationProvider(authProvider);
	}
	
}
