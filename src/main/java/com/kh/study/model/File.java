package com.kh.study.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name="FILES")
@SequenceGenerator(
	name = "FILE_SEQ_GENERATOR",
	sequenceName="FILE_SEQ", //테이블 생성시 만들어준 시퀀스 네임
	initialValue = 1,
	allocationSize = 1
	)
@NoArgsConstructor
@AllArgsConstructor
@Data
public class File {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="FILE_SEQ_GENERATOR")
	private long fileSeq;
	
	@Column(name="file_path")
	private String filePath;
	
	@Column(name="file_nm")
	private String fileNm;
	
	@Column(name="file_ch_nm")
	private String fileChNm;
	
	@Column(name="thum_yn")
	private String thumYn;
	
	@Column(name="reg_dt")
	private Date regDt;
	
	//파일(N) : 하우스(1) 연관관계 주인은 하우스
	@ManyToOne
	@JoinColumn(name="house_seq")
	private House house;
	
	@ManyToOne
	@JoinColumn(name="room_type_seq")
	private RoomType roomType;
}
