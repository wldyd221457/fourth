package com.kh.study.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity(name="user")
@Table(name="TOY_USER")
@SequenceGenerator(
	name = "USER_SEQ_GENERATOR",
	sequenceName="USER_SEQ", //테이블 생성시 만들어준 시퀀스 네임
	initialValue = 1,
	allocationSize = 1
	)
@NoArgsConstructor
@AllArgsConstructor
@Data
public class User {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="USER_SEQ_GENERATOR")
	private long userSeq;
	
	@Column(name="user_id")
	private String userId;
	
	@Column(name="user_pwd")
	private String userPwd;
	
	@Column(name="user_nm")
	private String userNm;
	
	@Column(name="user_ph")
	private String userPh;
	//등록 일시
	@Column(name="reg_dt")
	private Date userDt;
	//수정 일시
	@Column(name="mod_dt")
	private Date modDt;
	
	//사용자(1) : 하우스(N) 양방향 매칭 user에 의해 매칭됨(양방향 관계에서 user가 주인)
	@OneToMany(mappedBy = "user")
	private List<House> houses = new ArrayList<House>();
	
	@OneToMany(mappedBy = "user")
	private List<Book> books = new ArrayList<Book>();
	
	@OneToOne(mappedBy="user")
	private Auth auth;
	
}
