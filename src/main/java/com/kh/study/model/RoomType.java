package com.kh.study.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name="ROOM_TYPE")
@SequenceGenerator(
	name = "ROOM_TYPE_SEQ_GENERATOR",
	sequenceName="ROOMTYPE_SEQ", //테이블 생성시 만들어준 시퀀스 네임
	initialValue = 1,
	allocationSize = 1
	)
@NoArgsConstructor
@AllArgsConstructor
@Data
public class RoomType {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="ROOM_TYPE_SEQ_GENERATOR")
	private long roomTypeSeq;
	
	@Column(name="room_type_title")
	private String roomTypeTitle;
	
	@Column(name="reg_dt")
	private Date regDt;
	
	@Column(name="mod_dt")
	private Date modDt;
	
	@Column(name="room_type_content")
	private String roomTypeContent;
	
	@OneToMany(mappedBy="roomType")
	private List<Room> rooms = new ArrayList<Room>();
	
	@OneToMany(mappedBy="roomType")
	private List<RoomOption> roomOptions = new ArrayList<RoomOption>();
	
	@OneToMany(mappedBy="roomType")
	private List<File> files = new ArrayList<File>();
	
	@ManyToOne
	@JoinColumn(name="house_seq")
	private House house;
	
}
