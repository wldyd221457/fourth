package com.kh.study.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name="ROOM_OPTION")
@SequenceGenerator(
	name = "ROOM_OPTION_SEQ_GENERATOR",
	sequenceName="ROOMOPTION_SEQ", //테이블 생성시 만들어준 시퀀스 네임
	initialValue = 1,
	allocationSize = 1
	)
@NoArgsConstructor
@AllArgsConstructor
@Data
public class RoomOption {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="ROOM_OPTION_SEQ_GENERATOR")
	private long roomOptionSeq;
	
	@Column(name="room_option_type")
	private String roomOptionType;
	
	@Column(name="room_option_content")
	private String roomOprionContent;
	
	@ManyToOne
	@JoinColumn(name="room_type_seq")
	private RoomType roomType;	
}
