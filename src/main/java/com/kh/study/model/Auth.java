package com.kh.study.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity(name="auth")
@Table(name="AUTH")
@SequenceGenerator(
	name = "AUTH_SEQ_GENERATOR",
	sequenceName="AUTH_SEQ", //테이블 생성시 만들어준 시퀀스 네임
	initialValue = 1,
	allocationSize = 1
	)
@NoArgsConstructor
@AllArgsConstructor
@ToString(exclude = {"user"}) 
@Data
public class Auth {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="AUTH_SEQ_GENERATOR") //위에서 만들어준 generator 사용
	private long authSeq;
	
	@Column(name="auth_role")
	private String authRole;
	
	@OneToOne
	@JoinColumn(name="user_seq")
	private User user;
	
}
