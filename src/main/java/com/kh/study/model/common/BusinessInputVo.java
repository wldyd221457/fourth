package com.kh.study.model.common;

import lombok.Data;

@Data
public class BusinessInputVo {
	/** 페이지당 개수 */
	private int linesPercentPage = 0;
	/** 현재 페이지 */
	private int currentPage = 0;
	/** 페이지당 개수 */
	private int rowOffSet = 0;
	/**페이지 검색 시 시작 숫자 */
	private int rowFirstNum;
	/**페이지 검색 시 마지막 숫자 */	
	private int rowLastNum;
}
