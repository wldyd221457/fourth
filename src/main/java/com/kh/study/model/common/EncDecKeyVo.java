package com.kh.study.model.common;

import java.util.List;

public class EncDecKeyVo {

	private List<String> encKeyList;
	
	private String encKey;

	public List<String> getEncKeyList() {
		return encKeyList;
	}

	public void setEncKeyList(List<String> encKeyList) {
		this.encKeyList = encKeyList;
	}

	public String getEncKey() {
		return encKey;
	}

	public void setEncKey(String encKey) {
		this.encKey = encKey;
	}
}
