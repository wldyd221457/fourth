package com.kh.study.model.common;

import java.util.HashMap;
import java.util.Map;

/*
 * ajax 요청 시 결과 값 전달 객체
 * 
 */
public class ResultVo {

	private String resultCode;
	
	private String resultMsg;
	
	private Object resultObj;
	
	private Object resultObj2;
	
	/** 요청 처리 결과 값*/
	private Map<String, Object> result = null;	
	
	public ResultVo() {
	    result = new HashMap<String, Object>();		
	}
	
	public String getResultCode() {
		return resultCode;
	}
	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}
	public String getResultMsg() {
		return resultMsg;
	}
	public void setResultMsg(String resultMsg) {
		this.resultMsg = resultMsg;
	}
    public Object getResultObj() {
        return resultObj;
    }
    public void setResultObj(Object resultObj) {
        this.resultObj = resultObj;
    }
    public Object getResultObj2() {
		return resultObj2;
	}

	public void setResultObj2(Object resultObj2) {
		this.resultObj2 = resultObj2;
	}
	public Map<String, Object> getResult() {
	    return result;
	}
	public void setResult(Map<String, Object> result) {
	    this.result = result;
	}
	/**
	 * 요청 처리 결과 값을 추가 한다.
	 * @param key json형식의 요청 처리 결과 값을 key
	 * @param value json형식의 요청 처리 결과 값의 value
	 */
	public void addResult(String key, Object value) {
	    this.result.put(key, value);
	}	
	
	public void setResult(ResultCode resultCode) {
		this.resultCode = resultCode.getCode();
		this.resultMsg = resultCode.getDescription();
	}
}
