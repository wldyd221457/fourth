package com.kh.study.model.common;

public enum ResultCode {
	
	/*로그인 관련*/
	RESPONSE_LOGIN_USER_NOT_FOUND ("E01", "미등록 ID 입니다."),
	RESPONSE_LOGIN_INVALID_PASSWORD ("E05", "비밀번호 오류입니다."),
	
	/*SMS 인증 관련*/
	RESPONSE_SMS_RESPONSE_OK("0000", "인증번호를 확인하였습니다.");
	
	private String code;
	private String description;

	ResultCode(String code, String description) {
		this.code = code;
		this.description = description;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
}
