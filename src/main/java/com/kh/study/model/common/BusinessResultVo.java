package com.kh.study.model.common;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class BusinessResultVo {
	/** 페이지 개수 */
	private int listCount = 0;
	/** 전체 레코드 개수 */
	private int listCountTotal = 0;
	/** 상태 코드 */
	private String businessCode = null;
	/** 상태 메시지 */
	private String businessMessage = null;
	/** 호출 페이지 */
	private String businessCallPage = null;
	
	/**
	 * 페이지 개수를 반환한다.<BR/>
	 * 
	 * @return 페이지 개수
	 */
	@JsonIgnore
	public int getListCount() {
		return listCount;
	}

	/**
	 * 페이지 개수를 할당한다.<BR/>
	 * 
	 * @param listCount
	 *            페이지 개수
	 */
	public void setListCount(int listCount) {
		this.listCount = listCount;
	}

	/**
	 * 전체 레코드 개수를 반환한다.<BR/>
	 * 
	 * @return 전체 레코드 개수
	 */
	@JsonIgnore
	public int getListCountTotal() {
		return listCountTotal;
	}

	/**
	 * 전체 레코드 개수를 할당한다.<BR/>
	 * 
	 * @param listCountTotal
	 *            전체 레코드 개수
	 */
	public void setListCountTotal(int listCountTotal) {
		this.listCountTotal = listCountTotal;
	}

	/**
	 * 상태 코드를 반환한다.<BR/>
	 * 
	 * @return 상태 코드
	 */
	public String getBusinessCode() {
		return businessCode;
	}

	/**
	 * 상태 코드를 할당한다.<BR/>
	 * 
	 * @param businessCode
	 *            상태 코드
	 */
	public void setBusinessCode(String businessCode) {
		this.businessCode = businessCode;
	}

	/**
	 * 상태 메시지를 반환한다.<BR/>
	 * 
	 * @return 상태 메시지
	 */
	public String getBusinessMessage() {
		return businessMessage;
	}

	/**
	 * 상태 메시지를 할당한다.<BR/>
	 * 
	 * @param businessMessage
	 *            상태 메시지
	 */
	public void setBusinessMessage(String businessMessage) {
		this.businessMessage = businessMessage;
	}

	/**
	 * 호출 페이지를 반환한다.<BR/>
	 * 
	 * @return 호출 페이지
	 */
	public String getBusinessCallPage() {
		return businessCallPage;
	}

	/**
	 * 호출 페이지를 할당한다.<BR/>
	 * 
	 * @param businessCallPage
	 *            호출 페이지
	 */
	public void setBusinessCallPage(String businessCallPage) {
		this.businessCallPage = businessCallPage;
	}

	/**
	 * 클래스 변수의 정보를 반환한다.<BR/>
	 */
	@Override
	public String toString() {
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append("[BusinessResultVo] : ");
		stringBuffer.append("listCount=" + this.listCount);
		stringBuffer.append(", listCountTotal=" + this.listCountTotal);
		stringBuffer.append(", businessCode=" + this.businessCode);
		stringBuffer.append(", businessMessage=" + this.businessMessage);
		stringBuffer.append(", businessCallPage=" + this.businessCallPage);
		return stringBuffer.toString();
	}
}