/*
 * package com.kh.study.model;
 * 
 * import java.util.ArrayList; import java.util.List;
 * 
 * import javax.persistence.Column; import javax.persistence.Entity; import
 * javax.persistence.GeneratedValue; import javax.persistence.GenerationType;
 * import javax.persistence.Id; import javax.persistence.JoinColumn; import
 * javax.persistence.OneToMany; import javax.persistence.Table;
 * 
 * 학생 모델 작성자 : 인소진 
 * 수정사항 있으면 말해주세요
 * 샘플
 * @Entity
 * @Table(name ="tb_student") public class Student {
 * 
 * @Id
 * 
 * @Column(name = "student_no") private long studentNo;
 * 
 * @Column(name = "student_name", nullable = false) private String studentName;
 * 
 * @OneToMany
 * 
 * @JoinColumn(name = "student_no") private List<Board> boards = new
 * ArrayList<>();
 * 
 * public Student() {
 * 
 * } public Student(long studentNo, String studentName) { this.studentNo =
 * studentNo; this.studentName = studentName; }
 * 
 * 
 * public long getStudentNo() { return studentNo; }
 * 
 * public void setStudentNo(long studentNo) { this.studentNo = studentNo; }
 * 
 * 
 * public String getStudentName() { return studentName; } public void
 * setStudentName(String studentName) { this.studentName = studentName; }
 * 
 * @Override public String toString() { return "Student [ studentNo=" +
 * studentNo + ", studentName=" +studentName + "]"; }
 * 
 * }
 * 
 */