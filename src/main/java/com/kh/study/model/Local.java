package com.kh.study.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name="LOCALE")
@SequenceGenerator(
	name = "LOCAL_SEQ_GENERATOR",
	sequenceName="LOCAL_SEQ", //테이블 생성시 만들어준 시퀀스 네임
	initialValue = 1,
	allocationSize = 1
	)
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Local {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="LOCAL_SEQ_GENERATOR")
	private long localSeq;
	
	@OneToMany(mappedBy="local")
	private List<House> houses = new ArrayList<House>();
}
