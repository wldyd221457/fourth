package com.kh.study.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name="BOOK")
@SequenceGenerator(
	name = "BOOK_SEQ_GENERATOR",
	sequenceName="BOOM_SEQ", //테이블 생성시 만들어준 시퀀스 네임
	initialValue = 1,
	allocationSize = 1
	)
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Book {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="BOOK_SEQ_GENERATOR") //위에서 만들어준 generator 사용
	private long bookSeq;
	
	@Column(name="book_pay")
	private String bookPay;
	
	@Column(name="book_price")
	private String bookPrice;
	
	@ManyToOne
	@JoinColumn(name="user_seq")
	private User user;
	
	@ManyToOne
	@JoinColumn(name="room_seq")
	private Room room;
}
