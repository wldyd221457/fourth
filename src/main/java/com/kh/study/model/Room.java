package com.kh.study.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name="ROOM")
@SequenceGenerator(
	name = "ROOM_SEQ_GENERATOR",
	sequenceName="ROOM_SEQ", //테이블 생성시 만들어준 시퀀스 네임
	initialValue = 1,
	allocationSize = 1
	)
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Room {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="ROOM_SEQ_GENERATOR")
	private long RoomSeq;
	
	@Column(name="room_num")
	private String roomNum;
	
	@Column(name="room_status")
	private String roomStatus;
	
	@Column(name="root_start_dt")
	private Date roomStartDt;
	
	@Column(name="room_end_dt")
	private Date roomEndDt;
	
	@Column(name="reg_dt")
	private Date regDt;
	
	@ManyToOne
	@JoinColumn(name="room_type_seq")
	private RoomType roomType;
	
	@OneToMany(mappedBy="room")
	private List<Book> books = new ArrayList<Book>();
}
