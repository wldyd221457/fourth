package com.kh.study.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity(name="house")
@Table(name="HOUSE")
@SequenceGenerator(
	name = "HOUSE_SEQ_GENERATOR",
	sequenceName="HOUSE_SEQ", //테이블 생성시 만들어준 시퀀스 네임
	initialValue = 1,
	allocationSize = 1
	)
@Data
@NoArgsConstructor
@AllArgsConstructor
public class House {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="HOUSE_SEQ_GENERATOR")
	private long houseSeq;
	
	@Column(name="house_nm")
	private String houseNm;
	
	@Column(name="house_address")
	private String houseAddress;
	
	@Column(name="house_ph")
	private String housePh;
	
	@Column(name="house_content")
	private String houseContent;
	
	@Column(name="house_status")
	private String houseStatus;
	
	//하우스(N) : 사용자(1) 연관 관계 주인은 사용자
	@ManyToOne
	@JoinColumn(name="user_seq")
	private User user;
	
	//하우스(1) : 파일(N) 연관 관계 주인은 파일
	@OneToMany(mappedBy="house") 
	private List<File> files = new ArrayList<File>();
	 
	
	@ManyToOne
	@JoinColumn(name="local_seq")
	private Local local;
	
	@OneToMany(mappedBy="house")
	private List<RoomType> roomTypes = new ArrayList<RoomType>();
	
}
