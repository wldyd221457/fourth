package com.kh.study.repository;
 
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.*;

import javax.transaction.Transactional;

import com.kh.study.model.House;

public interface HouseRepository extends JpaRepository<House, Long>{
   @Query(value="SELECT h FROM house AS h WHERE houseSeq = :seq")
   House findOneByHouseSeqCustom(@Param("seq") long houseSeq);

   @Query(value="SELECT h FROM house AS h WHERE h.houseNm LIKE %:houseNm%")
   List<House> findByHouseNmCustom(@Param("houseNm") String houseNm);

   @Query(value="SELECT h FROM house AS h WHERE h.user.userNm LIKE %:userId% ")
   List<House> findByUserIdCustom(@Param("userId") String userId);

   @Modifying
   @Transactional
   @Query(value="UPDATE HOUSE SET HOUSE_STATUS = :hStatus WHERE HOUSE_SEQ = :hSeq", nativeQuery = true)
   int updateHouseStatusCustom(@Param("hStatus") String houseStatus, @Param("hSeq") long houseSeq);
}