package com.kh.study.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.kh.study.model.Book;

public interface BookRepository extends JpaRepository<Book, Long>{

}
