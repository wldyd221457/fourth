package com.kh.study.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.kh.study.model.User;

public interface UsersRepository extends JpaRepository<User, Long>{

	@Query("select u from user u where u.userId = :userId")
	User selectAdminUsersById(@Param("userId") String id);

}
