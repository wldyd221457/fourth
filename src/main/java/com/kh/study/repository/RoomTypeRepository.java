package com.kh.study.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.kh.study.model.RoomType;

public interface RoomTypeRepository extends JpaRepository<RoomType, Long>{

}
