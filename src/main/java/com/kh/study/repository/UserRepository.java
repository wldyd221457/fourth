package com.kh.study.repository;
 
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.kh.study.model.User;
import java.util.*;

public interface UserRepository extends JpaRepository<User, Long>{
   List<User> findAll();
   List<User> findByUserNmOrderByUserNm(String userNm);

   /*
      Query 설명
      @Query는 default가 nativeQuery=false이며, 이 상태에서는 기본적으로 JPQL을 사용하게 된다.
      JPQL은 간단히 말해 JPA를 구현했을 때 사용하게 된다. 
      테이블이 아닌 객체를 대상으로 쿼리를 실행하기 때문에
      "SELECT * FROM TOY_USER"를 하기 위해서는 
      "SELECT u FROM user AS u"와 같이 테이블 이름은 지정한 Entity명으로 해주고(대,소문자 구분), 별칭(Alias)도 지정해줘야한다.
      
      만약 기존에 SQL을 그대로 사용하고 싶다면 nativeQuery=true로 해주면 된다.
      */
   // @Query(value=" SELECT u FROM user u ")
   // List<User> findAllCustom();

   //삽질 기록 : type이 정상 등록되지 않아서 실패
   // @Query(value=" SELECT * FROM TOY_USER WHERE :type = :text", nativeQuery=true)
   // List<User> findByUserIdCustom(@Param("type")String searchType, @Param("text")String searchText);
   //삽질 기록 : type이 위와 같음 컬럼에는 적용이 안됨, desc도 안됨
   // @Query(value="SELECT * FROM TOY_USER WHERE user_id LIKE %:id% ORDER BY :column :type", nativeQuery=true)
   // List<User> findByUserIdLikeCustom(@Param("id") String searchText, @Param("column") String sortColumn, @Param("type") String sortType);
   // @Query(value="SELECT u FROM user u WHERE userId LIKE %:id% ORDER BY :type")
   // List<User> findByUserIdLikeCustom(@Param("id") String searchText, @Param("type") String sortType);
   
   /*
   몇 분 삽질하다가 알게된 뒤로 체념하고 쓰다가 문득 생각난건데 
   FROM에는 Entity로 정한 name쓰면서 WHERE절에서는 컬럼을 왜 name로 정한게 아니라 변수명으로 쓰는거야 왜..
   */
  //userId Like 조회, order by도 컬럼으로 써야되며, desc도 잘 먹힘
  @Query(value="SELECT u FROM user u WHERE userId LIKE %:id% ORDER BY userId, reg_dt")
  List<User> findByUserIdLikeCustom(@Param("id") String searchText);
  
   @Query(value="SELECT u FROM user u WHERE userNm LIKE %:id% ORDER BY userNm, reg_dt")
   List<User> findByUserNameLikeCustom(@Param("id") String searchText);
}