package com.kh.study.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.kh.study.model.House;

public interface HousesRepository extends JpaRepository<House, Long>{

	@Query("select count(h) from house h where h.user.userSeq = :userSeq")
	int count(@Param("userSeq") long userSeq);

	@Query("select h from house h where h.user.userSeq = :userSeq")
	List<House> getUserHousesList(@Param("userSeq") long userSeq);

	@Query("select h from house h where h.houseSeq = :houseSeq")
	House getUserDetailHouse(@Param("houseSeq") long houseSeq);

}
