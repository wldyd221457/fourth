package com.kh.study.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.kh.study.model.Local;

public interface LocalRepository extends JpaRepository<Local, Long>{

}
