package com.kh.study.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.kh.study.model.RoomOption;

public interface RoomOptionRepository extends JpaRepository<RoomOption, Long>{

}
