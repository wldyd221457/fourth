package com.kh.study.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.kh.study.model.File;

public interface FileRepository extends JpaRepository<File, Long>{

}
