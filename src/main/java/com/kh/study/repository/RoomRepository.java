package com.kh.study.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.kh.study.model.Room;

public interface RoomRepository extends JpaRepository<Room, Long>{

}
