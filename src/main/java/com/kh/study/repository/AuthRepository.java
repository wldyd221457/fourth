package com.kh.study.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.kh.study.model.Auth;

public interface AuthRepository extends JpaRepository<Auth, Long>{

}