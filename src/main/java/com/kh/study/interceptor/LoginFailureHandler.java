package com.kh.study.interceptor;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kh.study.exception.login.InvalidPasswordException;
import com.kh.study.model.common.ResultCode;
import com.kh.study.model.common.ResultVo;

@Component
public class LoginFailureHandler implements AuthenticationFailureHandler {

	@Override
	public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException exception) throws IOException, ServletException {

		ResultVo resultVo = new ResultVo();
		ObjectMapper mapper = new ObjectMapper();
		String jsonInString = null;
		
		if(exception instanceof UsernameNotFoundException){
			// 존재하지 않는 사용자
			resultVo.setResult(ResultCode.RESPONSE_LOGIN_USER_NOT_FOUND);
		} else if(exception instanceof InvalidPasswordException){   
			// 비밀번호 틀림
	        resultVo.setResult(ResultCode.RESPONSE_LOGIN_INVALID_PASSWORD);	            	            
		}
		
		jsonInString = mapper.writeValueAsString(resultVo);
		response.setContentType("application/json");
		response.setCharacterEncoding("utf-8");
		PrintWriter out = response.getWriter();
		out.print(jsonInString);
		out.flush();
		out.close();
	}

}
