package com.kh.study.interceptor;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kh.study.model.User;
import com.kh.study.model.common.ResultCode;
import com.kh.study.model.common.ResultVo;
import com.kh.study.util.Consts;

@Component
public class LoginSuccessHandler implements AuthenticationSuccessHandler{

	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
			Authentication authentication) throws IOException, ServletException {
		
		ResultVo resultVo = new ResultVo();
		ObjectMapper mapper = new ObjectMapper();
		String jsonInString = null;
		
		Object vo = authentication.getPrincipal();
		
		User userVo = (User) vo;
		HttpSession session = request.getSession();	
		
		session.setAttribute("sessionUser", userVo.getUserId());
		session.setAttribute("userNm", userVo.getUserNm());
		session.setAttribute("userSeq", userVo.getUserSeq());
		session.setAttribute("userIp", request.getRemoteAddr());
		
		// 권한 체크 후 default URL 세팅
		String adminAuth = userVo.getAuth().getAuthRole();
		String homeUrl = null;
		
		//추후 ADMIN 타입별 homeURL 수정
		if (adminAuth.equals(Consts.SUPER_ADMIN_ROLE_NAME) || 
					adminAuth.equals(Consts.BIZ_ADMIN_WITH_CHANNEL_ROLE_NAME) || 
					 adminAuth.equals(Consts.BIZ_ADMIN_NO_CHANNEL_ROLE_NAME)) {
			homeUrl = "/main";
		} else if (adminAuth.equals(Consts.STAT_ADMIN_ROLE_NAME)) {
			homeUrl = "/main";
		}
		session.setAttribute("homeUrl", homeUrl);
		
		resultVo.setResult(ResultCode.RESPONSE_SMS_RESPONSE_OK);
		resultVo.addResult("homeUrl", request.getContextPath() + homeUrl);
		jsonInString = mapper.writeValueAsString(resultVo);
		
		response.setContentType("application/json");
		response.setCharacterEncoding("utf-8");
		PrintWriter out = response.getWriter();
		
		out.print(jsonInString);
		out.flush();
		out.close();
	}

}
