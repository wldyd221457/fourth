package com.kh.study.interceptor;

import java.util.ArrayList;
import java.util.Collection;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.stereotype.Component;

import com.kh.study.exception.login.InvalidPasswordException;
import com.kh.study.model.User;
import com.kh.study.services.LoginService;
import com.kh.study.util.Consts;

@Component
public class AuthProvider implements AuthenticationProvider{
	
	@Autowired 
	HttpServletRequest request;

	@Autowired
	LoginService loginService;
	
	@Autowired
	BCryptPasswordEncoder passwordEncoder;

	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {

		//String userPwd = "usernm01";
		//System.out.println("유저 비밀번호     => " + passwordEncoder.encode(userPwd));
		
		//로그인 성공시 획득할 권한 리스트
        Collection<GrantedAuthority> authorities = new ArrayList<>();
        //유저 정보 객체
        User userVo = new User();
        //권한 정보 값이 담길 변수
        String authority = null;
    	
    	String id = (String) authentication.getPrincipal();
		String pw = (String) authentication.getCredentials();
        
		//유저 정보조회
        userVo = loginService.selectUsersById(id);

        // 아이디 존재하지 않는 경우
        if(userVo == null) {
        	throw new UsernameNotFoundException("");
        }
        
        //비밀번호 불일치
        if(!passwordEncoder.matches(pw, userVo.getUserPwd())) {
        	throw new InvalidPasswordException("");
        }
        
        authority = userVo.getAuth().getAuthRole();
        
        authorities.add(new SimpleGrantedAuthority(authority));
			
		return new UsernamePasswordAuthenticationToken(userVo, null, authorities);
	} 
	
	@Override
    public boolean supports(Class<?> authentication) {
		return true;
    }
    
}
