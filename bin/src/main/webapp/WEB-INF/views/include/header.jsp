<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link href="https://fonts.googleapis.com/css?family=Do+Hyeon&display=swap" rel="stylesheet">
<title>야놀자 헤더</title>
</head>
<style>

.headerDiv {
	width: 100%;
	height: 200px;
	background: #f7323f;
	overflow: hidden;
	text-align: center;
}

.logoOneDiv{
	display: inline_black;
	width: 70%;
	height: 40px;
	overflow: hidden;
	margin: auto;
}

.headerUlDiv{
	width: 50%;
	height: 100%;
	overflow: hidden;
	float: right;
}

.headerUlDiv ul{
	float: right;
}

.headerUl li{
	list-style-type: none;
	float: left;
}

.logoOneDiv li a {
	display: block;
	color: white;
	text-align: center;
	padding: 14px 16px;
	text-decoration: none;
}

.logoOneDiv #logo{
	width: 120px;
	height: 50px;
	float: left;
}

.logoOneDiv a{
	font-family: 'Do Hyeon', sans-serif;
	font-size: 24px;
	color: white;
}

.logoTwoDiv {
	margin: auto;
	width: 70%;
	height: 80%;
	overflow: hidden;
}

.logoTwoDiv img{
	float: right;
}

.logoTwoDiv p{
	float: left;
	font-family: 'Do Hyeon', sans-serif;
	font-size: 37px;
	margin-left: 10%;
	color: white;
}

.headerUlDiv ul, ol, li {
    list-style: none;
    margin-bottom: 16px;
    margin: 0;
    padding: 0;
    border: 0;
    box-sizing: border-box;
    visibility: visible;
}

</style>
<body>
	<div class="headerDiv">
		<div class="logoOneDiv">
			<img src="<c:url value="/resources/images/hader_logo.png" />" id="logo" style="cursor: pointer" alt="야놀자">
			<div class="headerUlDiv">
				<ul class="headerUl">
					<li><a class="active" href="#home">관리자</a></li>
					<li><a href="#news">판매자</a></li>
					<li><a href="#contact">사용자</a></li>
					<li><a href="#about">기타</a></li>
				</ul>
			</div>
		</div>
		<div class="logoTwoDiv">
			<p>호텔 예약사이트</p>
			<img src="<c:url value="/resources/images/header_plus.PNG" />" id="logo_plus" style="cursor: pointer" alt="야놀자">
		</div>
	</div>
</body>
</html>