<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/include/header.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>조가튼 세상</title>
</head>
<link href="https://fonts.googleapis.com/css?family=Do+Hyeon&display=swap" rel="stylesheet">
<script type="text/javascript" src="http://code.jquery.com/jquery-1.7.min.js">
</script>
<script type="text/javascript">
$(function(){
	// 클릭시 view 호출
	$(".stage_gra_black_vertical").click(function(){
		var encKey = $(this).find("input[type=hidden]").val();
		go_view(encKey, event);
	});
	
	$(".records").click(function(){
		var encKey = $(this).find("input[type=checkbox]").val();
		go_view(encKey, event);
	});
});

//상세 view 호출
function go_view(key, e){
	
	if(e != undefined && e != '') {
		if(e.target.localName == "input") {
			return;
		}
	}
	
	if(key != '') {
		$("#hiddenHouseKey").val(key);
	}else {
		$("#hiddenHouseKey").val("");
	}
	
	var frmData = document.form2;
	  
	frmData.action="<c:url value='/seller/seller_user_house_detail'/>"; 
	frmData.method="post"; 
	frmData.submit();
}

</script>
<style>
#contents{
	width: 100%;
	height: 100%;
	overflow: hidden;
	float: center;
}
#contents h3{
	margin-left: 20%;
}
.records {
	cursor: pointer;
}
.records:hover {
	background-color: #EEEEEE;
}
table, th, td {
	border: 1px solid #bcbcbc;
	margin-left: auto;
    margin-right: auto;
    text-align: center;
}
#content{
	width: 70%;
	height: 100%;
	float: center;
	margin: auto;
}

.rightDiv{
	float: right;
	width: 75%;
	height: 100%;
	overflow: hidden;
	margin: auto;
	border: 1px solid #f7323f;
	border-radius: 5px;
}

.stage_gra_black_vertical{
	width: 80%;
	height: 380px;
	border: 1px solid white;
	background-size: 100% 100%;
	margin: auto;
	cursor: pointer;
}

.badge, .stage{
	overflow: hidden;
	height: 18px;
    padding: 0 3px;
    font-size: 14px;
    line-height: 18px;
    margin: 40px 10px 0 24px;
}
.houseNm{
	font-family: 'Do Hyeon', sans-serif;
	font-size: 27px;
	color: white;
	margin: 0 10px 0 28px;
}
.score{
	font-family: 'Jua', sans-serif;
	font-size: 15px;
	color: #FF8000;
	margin: 15px 10px 10px 28px;
}
.addres{
	font-family: 'Do Hyeon', sans-serif;
	font-size: 27px;
	color: white;
	margin: 5px 10px 10px 28px;
}
.map_html{
	margin: 5px 9px 8px 28px;
}
.map_html p{
	font-family: 'Do Hyeon', sans-serif;
	font-size: 27px;
	color: white;
}

/* 왼쪽 사이드바 */
.filter_wrap {
    float: left;
    display: inline-block !important;
    position: relative !important;
    z-index: 1;
    width: 296px;
    height: 700px;
    /* border: 1px solid rgba(0,0,0,0.08); */
    border: 1px solid #FA5858;
    border-radius: 4px;
    background: #fff;
}
/* 초기화 버튼 */
.btn_wrap button:nth-child(1) {
    float: left;
    width: 147px;
    height: 40px;
    border: 1px solid rgb(242,7,76);
    background: #fff;
    color: rgb(242,7,76);
}
/* 적용 버튼 */
.btn_wrap button:nth-child(2) {
    float: right;
    width: 147px;
    height: 40px;
    border: 1px solid rgb(242,7,76);
    background: rgb(242,7,76);
    color: #fff;
}
/* 사이드바 체크 박스 */
.filter_wrap section {
    padding: 27px 0 0 0;
    border-bottom: none;
    box-sizing: border-box;
    visibility: visible;
}

</style>
<body>
	<div id="contents">
		<h3>호텔 관리 리스트</h3>
		<form id="form" method="post" name="form">
			<input type="hidden" name="encKeys" id="encKeys" /> <input
				type="hidden" name="encKey" id="encKey" /> <input type="hidden"
				id="listCountTotal" name="listCountTotal"
				value="<c:out value='${BusinessResultVo.listCountTotal}'/>" /> <input
				type="hidden" id="linesPercentPage" name="linesPercentPage"
				value="<c:out value='${BusinessInputVo.linesPercentPage}'/>" /> <input
				type="hidden" id="currentPage" name="currentPage"
				value="<c:out value='${BusinessInputVo.currentPage}'/>" />
			<div id="content" class="sub_wrap">
				<div class="filter_wrap">
					<h3>상세조건</h3>
					<div class="btn_wrap">
						<button type="button">초기화</button>
						<button type="apply">적용</button>
					</div>
					<section>
						<ul>
							<li><input type="checkbox" id="reserve_0" name="reserve[]"
								class="inp_chk" value="2"><label for="reserve_0"
								class="label_chk">예약 가능</label></li>
							<li><input type="checkbox" id="promotion_1"
								name="promotion[]" class="inp_chk" value="Y"><label
								for="promotion_1" class="label_chk">프로모션</label></li>
						</ul>
					</section>
				</div>
				<div class="rightDiv">
				<table class="board_list3">
					<tbody>
						<c:choose>
							<c:when test="${BusinessResultVo.listCountTotal > 0}">
								<c:set var="cnt_num"
									value="${BusinessResultVo.listCountTotal - (BusinessInputVo.linesPercentPage * (BusinessInputVo.currentPage - 1))}" />
								<c:set var="i" value="0" />
								<c:forEach items="${houseVos}" var="vo">
									<div class="stage_gra_black_vertical" style="background-image: URL(<c:url value="${vo.files.get(0).filePath }" />);">
											<form id="form2" name="form2" method="post">
												<input type="hidden" class="hiddenHouseKey" name="hiddenHouseKey" value="${EncDecKeyVo.encKeyList[i]}">
											</form>
										<div class="name">
											<div class="badge">
												<span class="build_badge"
													style="color: rgba(255, 255, 255, 1); background-color: rgba(106, 120, 141, 1);">3성급</span>
											</div>
											<strong class="houseNm"><c:out
													value="${vo.houseNm }" /></strong>
											<p class="score">
												<em>9.2</em>&nbsp;<span>추천해요</span>&nbsp;(384)
											</p>
											<p class="addres">
												<c:out value="${vo.houseAddress }" />
											</p>
										</div>
										<div class="price">
											<div class="map_html">
												<p>
													<em class="mark"><span><b>남은 객실 1개</b><i>&nbsp;</i></span></em><b>94,900원</b>
												</p>
											</div>
										</div>
									</div>
									<c:set var="i" value="${i + 1}" />
									<%-- <tr class="records">
									<td><input type="checkbox" name="checkList"
										value="${EncDecKeyVo.encKeyList[i]}"></td>
									<td class="clickable">${cnt_num}</td>
									<td class="clickable"><c:out value="${vo.houseNm }" /></td>
									<td class="clickable"><c:out value="${vo.houseAddress }" /></td>
									<td class="clickable"><c:out value="${vo.user.userNm }" /></td>
									<td class="clickable"><c:out value="${vo.modDate }" /></td>
								</tr>
								<c:set var="cnt_num" value="${cnt_num - 1}" />
								<c:set var="i" value="${i + 1}" /> --%>
								</c:forEach>
							</c:when>
							<c:otherwise>
								<tr>
									<td colspan="10">조회결과가 없습니다.</td>
								</tr>
							</c:otherwise>
						</c:choose>
					</tbody>
				</table>
				</div>
			</div>
		</form>
	</div>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
</body>
</html>