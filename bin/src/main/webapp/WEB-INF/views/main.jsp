<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>로그인 성공</title>
</head>
<body>
	<div class="stage gra_black_vertical">
		<div class="evt_info">
			<span>맛있는 건 함께 먹고 싶으니까~[조식 2인제공]</span>
		</div>
		<div class="name">
			<div class="badge">
				<span class="build_badge"
					style="color: rgba(255, 255, 255, 1); background-color: rgba(106, 120, 141, 1);">3성급</span>
			</div>
			<strong>신라스테이 서초</strong>
			<p class="score">
				<em>9.2</em>&nbsp;<span>추천해요</span>&nbsp;(384)
			</p>
			<p>서초구</p>
		</div>
		<div class="price">
			<div class="map_html">
				<p>
					<em class="mark"><span><b>남은 객실 1개</b><i>&nbsp;</i></span></em><b>94,900원</b>
				</p>
			</div>
			<p>
				<em class="mark"><span><b>남은 객실 1개</b><i>&nbsp;</i></span></em><b>94,900원</b>
			</p>
		</div>
	</div>
</body>
</html>