<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>
	<meta charset="UTF-8">
	<title>야놀자 헤더</title>
</head>
<style>
	ul {
		list-style-type: none;
		margin: 0;
		padding: 0;
		background-color: #333;
	}

	ul:after {
		content: '';
		display: block;
		clear: both;
	}

	li {
		float: left;
	}

	li a {
		display: block;
		color: white;
		text-align: center;
		padding: 14px 16px;
		text-decoration: none;
	}

	li a:hover:not(.active) {
		background-color: #111;
	}

	.active {
		background-color: #4CAF50;
	}
</style>

<body>
	<ul class="nav1">
		<li><a id="admin" href="/study/admin/home">관리자</a></li>
		<li><a href="#news">판매자</a></li>
		<li><a href="#contact">사용자</a></li>
		<li><a href="#about">기타</a></li>
	</ul>
</body>

</html>

<script src="http://code.jquery.com/jquery-1.7.min.js"></script>

<script type="text/javascript">
	$(function () {
		var url = document.location.href.split('?')[0].split('/');
		var navs = $('.nav1 a');
		for (var i = 0; i < navs.length; i++) {
			if (navs[i].href.indexOf(url[url.length-(url.length-4)]) > 0 ) {
				$('#' + navs[i].id).attr('class', 'active');
			}
		}
	});
</script>