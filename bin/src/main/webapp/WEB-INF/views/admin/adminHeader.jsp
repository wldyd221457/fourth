<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>
	<meta charset="UTF-8">
	<title>어드민 헤더</title>
</head>
<style>
	.admin ul {
		list-style-type: none;
		margin: 0;
		padding: 0;
		background-color: #333;
	}

	.admin ul:after {
		content: '';
		display: block;
		clear: both;
	}

	.admin li {
		float: left;
	}

	.admin li a {
		display: block;
		color: white;
		text-align: center;
		padding: 14px 16px;
		text-decoration: none;
	}

	.admin li a:hover:not(.active) {
		background-color: gray;
		font-weight:bold;
	}

	.admin .active {
		background-color: darkgray;
	}
</style>

<body>
	<ul class="admin nav">
		<li><a id="manageAuth" href="/study/admin/manageAuth/userList">유저권한 관리</a></li>
		<li><a id="manageHouse" href="/study/admin/manageHouse/houseList">숙소 관리</a></li>
		<li><a href="#contact">카테고리 관리</a></li>
		<li><a href="#about">설정</a></li>
	</ul>
</body>

</html>

<script type="text/javascript">
	$(function () {
		var url = document.location.href.split('?')[0].split('/');

		var adminNavs = $('.admin.nav a');
		for (var i = 0; i < adminNavs.length; i++) {
			if (adminNavs[i].href.indexOf(url[url.length-(url.length-5)]) > 0) {
				$('#' + adminNavs[i].id).attr('class', 'active');
			}
		}
	});
</script>