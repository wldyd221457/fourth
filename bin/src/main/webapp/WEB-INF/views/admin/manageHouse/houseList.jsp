<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>관리자</title>
</head>

<style>
    .houseTb { border-collapse: collapse; text-align:center;}
    tr, th, td { border:1px solid black; width:50px;}   
    td { width:250px;}
    .wd70{width:70px;}
    #searchDiv { margin: 10px 0 10px 0;}
    #searchDiv select { height:30px;}
    #searchDiv input[type=text] { height:21px;}

</style>
<body>
    <jsp:include page="/WEB-INF/views/common/header.jsp"/>
    <jsp:include page="/WEB-INF/views/study/admin/adminHeader.jsp"/>
    
    <h2>숙소리스트</h2>

    <div id="searchDiv">
        <form id="searchUser" action="/study/admin/manageHouse/houseList" method="POST">
           <select name="searchType" id="searchType">
               <option <c:if test="${ searchType == 'id' }" > selected </c:if> value="id">등록자 ID로 검색</option>
               <option <c:if test="${ searchType == 'title' }" > selected </c:if> value="title">숙소명으로 검색</option>
           </select>
           <input type="text" name="searchText" id="searchText" value="<c:if test='${ !empty searchText }' > ${searchText}</c:if>" >
           <input type="submit" value="검색">
       </form>
    </div>

    <c:choose>
        <c:when test="${!empty houseList}">
            <table class="houseTb">
                <tr>
                    <th>등록번호</th>
                    <th>숙소이름</th>
                    <th>주소</th>
                    <th>승인여부</th>
                    <th>등록자</th>
                </tr>
                <c:forEach var="house" items="${houseList}" >
                    <tr>
                        <td class="wd70">${house.houseSeq}</td>
                        <td><a href="/study/admin/manageHouse/houseModify?houseSeq=${house.houseSeq}">${house.houseNm}</a></td>
                        <td>${house.houseAddress}</td>
                        <td>
                            <select name="houseStatus" id="houseStatus"> ${house.houseStatus}
                                    <option <c:if test="${!empty house.houseStatus}"> <c:if test="${ house.houseStatus == 0 }" > selected </c:if></c:if> value="0">대기중</option>
                                    <option <c:if test="${!empty house.houseStatus}"> <c:if test="${ house.houseStatus == 1 }" > selected </c:if></c:if> value="1">승인완료</option>
                                    <option <c:if test="${!empty house.houseStatus}"> <c:if test="${ house.houseStatus == 2 }" > selected </c:if></c:if> value="2">보류</option>
                                    <option <c:if test="${!empty house.houseStatus}"> <c:if test="${ house.houseStatus == 3 }" > selected </c:if></c:if> value="3">거절</option>
                            </select>
                        </td>
                        <c:if test="${!empty house.user}">
                            <td>${house.user.userId}</td>
                        </c:if>
                    </tr>
                </c:forEach>
            </table>
        </c:when>

        <c:otherwise>
                <h2>조회 결과가 없습니다.</h2>
        </c:otherwise>
    </c:choose>
</body>
</html>

<script type="text/javascript">

</script>
