<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>관리자</title>
</head>

<style>
    .houseTb { border-collapse: collapse; }
    tr, th, td { border:1px solid black; }   
    th { width:100px; font-size:1.2em; font-weight:bold; background-color:darkgray; color:white; text-align:center;}
    td { width:300px; text-align:left;}

</style>
<body>
    <jsp:include page="/WEB-INF/views/common/header.jsp"/>
    <jsp:include page="/WEB-INF/views/study/admin/adminHeader.jsp"/>
    <h2>House 상세페이지</h2>

    <table class="houseTb">
        <tr>
            <th>등록번호</th><td>${house.houseSeq}</td>
            <th>숙소이름</th><td>${house.houseNm}</td>
        </tr>
        <tr>
            <th>주소</th><td colspan=4>${house.houseAddress}</td>
        </tr>
        <tr>
            <th>등록자</th>
            <td>
                <c:if test="${!empty house.user}">${house.user.userNm}</c:if>
            </td>
            <th>연락처</th>
            <td>
                <c:if test="${!empty house.housePh}">${house.housePh}</c:if>
            </td>
        </tr>
        <tr>
            <th>지역</th>
            <td>
                <c:if test="${!empty house.local}">${house.local.localSeq}</c:if>
            </td>
            <th>방 정보</th>
            <td>
                <c:if test="${!empty house.roomTypes}">${house.roomTypes.get(0).roomTypeTitle}</c:if>
            </td>
        </tr>
        <tr>
            <th colspan=1 >방 이미지</th>
            <td colspan=3 >
                <c:if test="${!empty house.files}">
                    <img src="${house.files.get(0).filePath}"/>
                </c:if>
            </td>
        </tr>
        <tr>
            <th colspan=4>소개글</th>
        </tr>
        <tr>
            <td colspan=4 rowspan=3>
                <c:if test="${!empty house.user}">${house.houseContent}</c:if>
            </td>
        </tr>
    </table>

</body>
</html>

<script type="text/javascript">

</script>
