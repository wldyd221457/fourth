<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<style>
	ul {
    list-style-type: none;
    margin: 0;
    padding: 0;
    background-color: #333;
	}
	ul:after{
	    content:'';
	    display: block;
	    clear:both;
	}
	li {
	    float: left;
	}
	li a {
	    display: block;
	    color: white;
	    text-align: center;
	    padding: 14px 16px;
	    text-decoration: none;
	}
	li a:hover:not(.active) {
	    background-color: #111;
	}
	.active {
	    background-color: #4CAF50;
	}
</style>
<body>
	<ul>
      <li><a class="active" href="#home">관리자</a></li>
      <li><a href="#news">판매자</a></li>
      <li><a href="user">사용자</a></li>
      <li><a href="#about">기타</a></li>
    </ul>
</body>
</html>